'use strict';

var HttpError = require('../errorConstructor/httpError').HttpError;

// load user from cookie at each request
module.exports = function (req, res, next) {
	if (req.isAuthenticated()) { return next(); }
	next(new HttpError(401, 'Authentication required'));
};
