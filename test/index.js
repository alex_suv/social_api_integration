'use strict';

var expect = require("chai").expect;
var request = require("supertest");
var Account = require('./userSchema').Account;
var server = require('../lib/server');
var findUserBySession = require('../handlers/auth/findUserBySession').findUserBySession;
var api = require('../passport/api');



describe ('user schema methods', function () {

	beforeEach(function (done) {
		var user = new Account({
			email: 'test@gmail.com',
			password: 'test'
		});
		user.save(function (err) {
			if(err) done(err);
			done();
		})
	});

	afterEach(function (done) {
		Account.remove({email: 'test@gmail.com'}, function () {
			done();
		});

	});

	it('should hash password on save', function (done) {
		Account.findOne({email: 'test@gmail.com'})
			.exec(function (err, user) {
				if (err) return done(err);
				expect(user).to.be.a('object');
				expect(user).to.have.property('password').to.be.a('string');
				expect(user).to.have.property('password').with.length(60);
				done();
			})
	});

	it('should have compare password method', function (done) {
		Account.findOne({email: 'test@gmail.com'})
			.exec(function (err, user) {
				if (err) return done(err);
				user.comparePassword('test', function (err, isMatch) {
					if (err) return done(err);
					expect(isMatch).to.be.true;

					user.comparePassword('wrongPass', function (err, isMatch) {
						if (err) return done(err);
						expect(isMatch).to.be.false;
						done();
					});
				});
			})
	})
});

describe ('handlers', function () {

	describe('findUserBySession', function () {
		var requestJson;

		beforeEach(function () {
			requestJson = request(server).get('/findUserBySession');
		});

		it('should return isLogged:false', function (done) {
			requestJson
				.expect('Content-Type', /json/)
				.expect(200, function (err, res) {
					if (err) return done(err);
					expect(res.body.isLogged).to.equal(false);
					done();
				});
		})
	});

	describe('sign Up', function () {
		var requestJson;
		var user = {
			email: 'test@gmail.com',
			password: 'test'
		};

		beforeEach(function () {
			requestJson = request(server)
				.post('/signUp')
				.send(user);
		});

		afterEach(function (done) {
			Account.remove({email: user.email}, function () {
				done();
			});
		});

		it('should save user with original email', function (done) {
			requestJson
				.expect('Content-Type', /json/)
				.expect(200, function (err, res) {
					if (err) return done(err);
					expect(res.body.success).to.equal(true);
					expect(res.body.user.providerId).to.equal('local:test@gmail.com');
					done();
				});
		});

		it('shouldn\'t save user with unoriginal email', function (done) {
			request(server)
				.post('/signUp')
				.send(user)
				.expect(200, function (err, res) {

					requestJson
						.expect('Content-Type', /json/)
						.expect(200, function (err, res) {
							if (err) return done(err);
							expect(res.body.success).to.equal(false);
							expect(res.body.body).to.equal('Email already token');
							done();
						});

				});
						
		})
	});

	describe('logIn', function () {
		var requestJson;
		var user = {
			email: 'test@gmail.com',
			password: 'test'
		};

		afterEach(function (done) {
			Account.remove({email: 'test@gmail.com'}, function () {
				done();
			})
		});

		it('shouldn\'t login user if email is not exists', function (done) {
			request(server)
				.post('/auth/login')
				.send(user)
				.expect(200, function (err, res) {
					if(err) return done(err);
					expect(res.body.success).to.equal(false);
					expect(res.body.body).to.equal('Incorrect email or password');
					done();
				})
		});

		it('should login user if email and pass are match', function (done) {

			var profile = new Account({
				email: 'test@gmail.com',
				password: 'test',
				providerId: 'local:test@gmail.com',
				provider: 'local'
			});

			profile.save(function () {
				request(server)
					.post('/auth/login')
					.send(user)
					.expect(200, function (err, res) {
						if(err) return done(err);
						expect(res.body.success).to.equal(true);
						expect(res.body.user).to.be.an('object');
						done();
					})
			});

		});

		it('shouldn\'t login user if password is wrong', function (done) {

			var profile = new Account({
				email: 'test@gmail.com',
				password: 'test',
				providerId: 'local:test@gmail.com',
				provider: 'local'
			});

			user['password'] = 'wrongPassWord';

			profile.save(function () {
				request(server)
					.post('/auth/login')
					.send(user)
					.expect(200, function (err, res) {
						if(err) return done(err);
						expect(res.body.success).to.equal(false);
						expect(res.body.body).to.equal('Incorrect email or password');
						done();
					})
			});

		})

	})
});

describe('api', function () {
	describe('findOrCreateUser', function() {
		afterEach(function (done) {
			Account.remove({email: 'test@gmail.com'}, function () {
				done();
			});
		});

		var user = {
			emails: [{value:'test@gmail.com'}],
			_json: {image: {url: 'test'}},
			provider: 'google',
			id: '000000'
		};

		it('should save user with correct provider Id', function (done) {
			api.findOrCreateUser(user, 'google', null, function(err, user) {
				if(err) return done(err);
				expect(user).to.be.a('object');
				expect(user).have.property('email').to.equal('test@gmail.com');
				expect(user).have.property('providerId').to.equal('google:000000');
				expect(user).have.property('photo').to.equal('test');
				done();
			})
		});
	})
});
