'use strict';

var gfs = require('../db_provider/gridfs').gfs;
var Account = require('../db_provider/userSchema').Account;
var gridform = require('../db_provider/gridfs').form;
var Album = require('../db_provider/albumSchema').Album;
var HttpError = require('../errorConstructor/httpError').HttpError;

// get
exports.get = function (req, res, next) {
	var dbQuery = {};
	var key = Object.keys(req.query)[0];
	switch (key) {
		case 'albumId':
			dbQuery ={_id: req.query.albumId};
			break;
	}

	Album
		.findOne(dbQuery)
		.exec(function (err, album) {
			if (err) return next(err);
			gfs.files.find({_id:{$in: album.photos}}).toArray(function (err, file) {
				if (err) return next(err);
				res.json(file);
			});
		});

};

// upload
exports.post = function (req, res, next) {
	console.log('I received an upload request');
	var albumId;
	var form = gridform();
	form.maxFieldsSize = 10 * 1024 * 1024;
	form.multiples = true;

	form.on('field', function (name, value) {
		switch (name) {
			case 'albumId':
				albumId = value;
				break;
			default:
				return;
		}
	});

	form.on('file', function (name, file) {
		Album.update({_id: albumId},
			{
				$push: {photos: file.id},
				$inc: {quantity: 1},
				$set:{thumbnail:'/image?'+'filename=' + file.name}
			},
			{upsert: false}, function (err, m) {
				if (err) return next(err);
			});
	});
	form.parse(req, function (err, fields, files) {
		if (err) return next(err);
		res.json({success: 'ok'});
	});
};