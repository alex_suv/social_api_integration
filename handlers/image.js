'use strict';

var gfs = require('../db_provider/gridfs').gfs;
var HttpError = require('../errorConstructor/httpError').HttpError;

// get
exports.get = function (req, res, next) {
	var readstream = gfs.createReadStream({
		filename: req.query.filename
	});
	readstream.pipe(res);
};

