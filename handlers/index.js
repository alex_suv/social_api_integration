'use strict';

var express = require('express');
var router = express.Router();
var checkAuth = require('../middleware/checkAuth');
//routes
router.get('/partials/homePage.html', checkAuth);
//serve images
router.get('/image', checkAuth, require('./image').get);
//upload photos
router.post('/photo', checkAuth, require('./photo').post);
//serve photo metadata
router.get('/photo', checkAuth, require('./photo').get);
//create new albums
router.put('/album', checkAuth, require('./album').put);
//res with albums
router.get('/album', checkAuth, require('./album').get);
module.exports = router;
