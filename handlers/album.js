var HttpError = require('../errorConstructor/httpError').HttpError;
var Account = require('../db_provider/userSchema').Account;
var Album = require('../db_provider/albumSchema').Album;


exports.put = function (req, res, next) {
	var body = req.body;

	var newAlbum = new Album({
		title: body.title,
		description: body.description,
		owner: req.user.id
	});

	newAlbum.save(function (err, album) {
		if (err) return next(new HttpError(500, 'Error during saving new album'));

		Account
			.update({_id: req.user._id}, {
				$push: {'albums': album._id}
			}, {upsert: false})
			.exec(function (err, account) {
				if (err) return next(new HttpError(500, 'Error during dbsearch (album)'));
				res.send('ok');
			})

	});
};


exports.get = function (req, res, next) {
	Album
		.aggregate(
		{$match: {_id: {$in: req.user.albums}}},
		{$sort: {date: 1}}
		)
		.exec(function (err, albums) {
			if (err) return new HttpError(500, "Error during getting albums");
			console.log(albums);
			res.json(albums);
		})
};