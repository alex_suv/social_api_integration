'use strict';

// sign Out request
exports.signOut = function (req,res) {
	console.log('I received a Log Out post request');

	req.session.destroy();
	req.logout();
	req.user = res.locals.user = null;

	res.json({success: true});
};
