'use strict';

var express = require('express');
var router = express.Router();
var passport = require('passport');
var config = require('../../config/config');
var redirectHost = config.get('redirectHost');
var checkAuth = require('../../middleware/checkAuth');

//routes
router.get('/findUserBySession',checkAuth, require('./findUserBySession').findUserBySession);

router.post('/signUp', require('./signUp').signUp);
router.get('/signOut', require('./signOut').signOut);

router.post('/auth/login', function (req, res, next) {
	passport.authenticate('local', function (err, user, message) {
		var answer = {};
		if(err) next(err);
		if (user) {
			answer = {
				success: true,
				user: user
			};
			req.logIn(user, function (err) {
				if (err) {return next(err)}
				res.json(answer);
			});

		} else {
			answer = {
				success: false,
				body: message
			};
			res.json(answer);
		}
	})(req, res, next)
});

router.get('/auth/google',
	passport.authenticate('google', { scope: ['profile', 'email'] }));

router.get('/auth/google/callback',
	passport.authenticate('google', { failureRedirect: redirectHost + '/signUp' }),
	function(req, res) {
		res.redirect(redirectHost + '/home');
	});

router.get('/auth/vkontakte',
	passport.authenticate('vkontakte', {scope:['email', 'friends']}));

router.get('/auth/vkontakte/callback',
	passport.authenticate('vkontakte', { failureRedirect: redirectHost + '/signUp' }),
	function(req, res) {
		res.redirect(redirectHost + '/home');
	});

router.get('/auth/github',
	passport.authenticate('github'));

router.get('/auth/github/callback',
	passport.authenticate('github', { failureRedirect: redirectHost + '/signUp' }),
	function(req, res) {
		res.redirect(redirectHost + '/home');
	});

router.get('/auth/facebook',
	passport.authenticate('facebook', {scope:['email', 'public_profile' , 'user_friends']}));

router.get('/auth/facebook/callback',
	passport.authenticate('facebook', { failureRedirect: redirectHost + '/signUp' }),
	function(req, res) {
		res.redirect(redirectHost + '/home');
	});

module.exports = router;
