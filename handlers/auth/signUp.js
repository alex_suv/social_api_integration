'use strict';

var HttpError = require('../../errorConstructor/httpError').HttpError;
var api = require('../../passport/api');

exports.signUp = function (req, res, next) {
	var body = req.body;
	var answer;
	var profile = {
		provider: 'local',
		id: req.body.email,
		email: req.body.email,
		photo: '',
		password: req.body.password
	};
	var id = api.prepareId(profile);
	api.findUser(id, function (err, user) {
		if (err) return next(new HttpError(500));
		if (user) {

			answer = {
				success: false,
				body: 'Email already token',
				user: {}
			};
			res.json(answer);
		} else {
			api.storeUser(profile, 'local', function (err, user) {
				if (err) return next(new HttpError(500));
				if (user) {
					answer = {
						success: true,
						body: '',
						user: user
					};
					req.login(user, function (err) {
						if (err) return next(new HttpError(500));
						res.json(answer);
					})
				}
			})
		}
	})
};
