'use strict';

exports.findUserBySession = function (req, res, next) {
	var answer = {};
	// if client's cookie linked with user _id send res with user doc if not respond with error 401
	if(req.user) {
		answer = {
			user: req.user,
			isLogged: true
		};
		res.json(answer);
	} else {
		answer = {
			isLogged: false
		};
		res.json(answer);
	}
};
