'use strict';

var server = require('./lib/server');
var config = require('./config/config');
var port = config.get('bind.port');
// start the http server
server.listen(port, function () {
	console.log("Http server listening on port " + port);
});
