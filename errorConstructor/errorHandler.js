var HttpError = require('../errorConstructor/httpError').HttpError;
var config = require('../config/config');

module.exports = function (err, req, res, next) {
	if (typeof err == 'number') {
		err = new HttpError(err);
	}
	if (err instanceof HttpError) {
		console.error('instance of HttpError errorConstructor');
		console.error(err);
		res.sendHttpError(err);
	} else {
		if (config.get('env') === 'development') {
			console.error('development errorConstructor');
			console.error(err);
			next(err);
		} else {
			console.error('production errorConstructor');
			console.error(err);
			next(err);
		}
	}
};
