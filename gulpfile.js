'use strict';

var gulp = require('gulp'),
	livereload = require('gulp-livereload'),
	nodemon = require('gulp-nodemon'),
	notify = require("gulp-notify");

gulp.task('reload', function () {
	gulp.src(['./public/index.html'])
		.pipe(livereload())
		.pipe(notify({message: 'Reloaded', time: 500, sound: false}));
});

gulp.task('watch_public', function () {

	gulp.watch(['./public/**/*.html'], ['reload']);

	gulp.watch(['./public/js/**/*.js','./public/js/HomeMaps.js'], function (files) {
		livereload.changed(files)
	});

	gulp.watch(['./public/css/**/*.css'], function (files) {
		livereload.changed(files)
	});

});


gulp.task('start_nodemon', function () {
	livereload.listen();
	nodemon({
		script: 'index.js',
		ext: 'js',
		env: {'NODE_ENV': 'development'},
		ignore: ['node_modules/', 'public/', 'gulpfile.js']
	})
		.once('start', function () {
			setTimeout(function () {
				gulp.src(['./public/index.html'])
					.pipe(livereload());
			}, 5000)
		})
		.on('restart', function () {
			setTimeout(function () {
				gulp.src(['./public/index.html'])
					.pipe(livereload());
			}, 4000)
		});
});

gulp.task('default', ['watch_public', 'start_nodemon']);
