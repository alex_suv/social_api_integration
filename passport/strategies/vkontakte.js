'use strict';

var VKontakteStrategy = require('passport-vkontakte').Strategy;
var config = require('../../config/config');
var VKONTAKTE_APP_ID = config.get('auth.vkontakte.VKONTAKTE_APP_ID');
var VKONTAKTE_APP_SECRET = config.get('auth.vkontakte.VKONTAKTE_APP_SECRET');

var api = require('./../api');
var strategy = 'vkontakte';

module.exports = new VKontakteStrategy({
		clientID: VKONTAKTE_APP_ID,
		clientSecret: VKONTAKTE_APP_SECRET,
		callbackURL: "http://localhost:8888/auth/vkontakte/callback",
		passReqToCallback: true,
		apiVersion: '5.34',
		profileFields: ['city', 'bdate', 'email', 'friends']
	},
	function (req, accessToken, refreshToken, params, profile, done) {
		process.nextTick(function () {
			api.findOrCreateUser(profile, strategy, params.email, done);
			return done(null, profile);
		});
	}
);
