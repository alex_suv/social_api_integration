'use strict';

var GoogleStrategy = require('passport-google-oauth').OAuth2Strategy;
var config = require('../../config/config');
var GOOGLE_CLIENT_ID = config.get('auth.google.GOOGLE_CLIENT_ID');
var GOOGLE_CLIENT_SECRET = config.get('auth.google.GOOGLE_CLIENT_SECRET');

var api = require('./../api');
var strategy = 'google';

module.exports = new GoogleStrategy({
		clientID: GOOGLE_CLIENT_ID,
		clientSecret: GOOGLE_CLIENT_SECRET,
		callbackURL: "http://localhost:8888/auth/google/callback"
	},
	function(accessToken, refreshToken, params, profile , done) {
		process.nextTick(function () {
			api.findOrCreateUser(profile, strategy, params, done);
			return done(null, profile);
		});
	}
);
