'use strict';

var LocalStrategy = require('passport-local').Strategy;

var api = require('../api');
var strategy = 'local';

module.exports = new LocalStrategy({
	usernameField: 'email',
	passwordField: 'password',
	passReqToCallback: true
},function (req, username, password, done) {
	var providerId = strategy + ':' + username;
	api.findUser(providerId, function (err, user) {
		if (err) return done(err);
		if (user) {
			api.checkPassword(user, password, function (err, isMatch) {
				if(isMatch) {
					done(null, user);
				} else {
					done(null, false, 'Incorrect email or password');
				}
			});
		} else {
			done(null, false, 'Incorrect email or password');
		}
	})
});
