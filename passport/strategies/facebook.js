'use strict';

var FacebookStrategy = require('passport-facebook').Strategy;
var config = require('../../config/config');
var FACEBOOK_APP_ID = config.get('auth.facebook.FACEBOOK_APP_ID');
var FACEBOOK_APP_SECRET = config.get('auth.facebook.FACEBOOK_APP_SECRET');

var api = require('./../api');
var strategy = 'facebook';

module.exports = new FacebookStrategy({
		clientID: FACEBOOK_APP_ID,
		clientSecret: FACEBOOK_APP_SECRET,
		callbackURL: "http://localhost:8888/auth/facebook/callback",
		profileFields: ['email']
	},
	function(accessToken, refreshToken, params, profile, done) {
		process.nextTick(function () {
			api.findOrCreateUser(profile, strategy, params, done);
			return done(null, profile);
		});
	}
);
