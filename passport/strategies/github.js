'use strict';

var GitHubStrategy = require('passport-github').Strategy;
var config = require('../../config/config');
var GITHUB_CLIENT_ID = config.get('auth.github.GITHUB_CLIENT_ID');
var GITHUB_CLIENT_SECRET = config.get('auth.github.GITHUB_CLIENT_SECRET');

var api = require('./../api');
var strategy = 'github';

module.exports = new GitHubStrategy({
		clientID: GITHUB_CLIENT_ID,
		clientSecret: GITHUB_CLIENT_SECRET,
		callbackURL: "http://localhost:8888/auth/github/callback"
	},
	function(accessToken, refreshToken, params, profile, done) {
		process.nextTick(function () {
			api.findOrCreateUser(profile, strategy, params, done);
			return done(null, profile);
		});
	}
);
