'use strict';

var Account = require('../db_provider/userSchema').Account;

function prepareId(profile) {
	return [profile.provider, profile.id].join(':');
}

function findOrCreateUser(profile, strategy, paramsEmail, callback) {
	var id = prepareId(profile);
	switch (strategy) {
		case 'google':
			profile.email = profile.emails[0].value;
			profile.photo = profile._json.image.url;
			break;
		case 'vkontakte':
			profile.email = paramsEmail;
			profile.photo = profile._json.photo;
			break;
		case 'facebook':
			profile.email = profile.emails[0].value;
			profile.photo = profile._json.photo;
			break;
		case 'github':
			profile.email = profile.emails[0].value;
			profile.photo = profile._json.avatar_url;
	}

	findUser(id, onFetchedUser);

	function onFetchedUser(err, user) {
		if (err) {return callback(err);}
		if (user) {
			return callback(null, user);
		} else {

			return storeUser(profile, strategy, callback);
		}
	}
}

function findUser(id, callback) {
	Account.findOne({providerId: id}).exec(function (err, user) {
		if (err) {
			return callback(err);
		} else if (!user) {
			return callback(null, null);
		} else {
			return callback(null, user);
		}
	})
}

function storeUser(profile, strategy, callback) {
	var id = prepareId(profile);
	var user = new Account({
		email: profile.email,
		providerId: id,
		provider: strategy,
		photo: profile.photo
	});

	if(strategy === 'local') {
		user['password'] = profile.password;
	}
	user[strategy] = profile;
	user.save(function (err, user) {
		if (err) {return callback(err);}
		else {return callback(null, user);}
	})
}

function serializeUser(user, callback) {
	if(user.provider === 'local'){
		callback(null, user.providerId)
	} else {
		callback(null, prepareId(user));
	}
}

function checkPassword (user, password, cb) {
	user.comparePassword(password, function (err, isMatch) {
		if (err) return cb(null, false);
		return cb(null, isMatch);
	});
}

module.exports = {
	findOrCreateUser: findOrCreateUser,
	serializeUser: serializeUser,
	deserializeUser: findUser,
	storeUser: storeUser,
	findUser: findUser,
	checkPassword: checkPassword,
	prepareId: prepareId
};
