'use strict';

var local = require('./strategies/local');
var google = require('./strategies/google');
var vkontakte = require('./strategies/vkontakte');
var github = require('./strategies/github');
var facebook = require('./strategies/facebook');
var api = require('./api');

module.exports = function(passport) {

	passport.serializeUser(api.serializeUser);

	passport.deserializeUser(api.deserializeUser);

	passport.use(local);
	passport.use(google);
	passport.use(vkontakte);
	passport.use(github);
	passport.use(facebook);
};
