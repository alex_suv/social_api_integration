'use strict';

var config = require('../config/config');
var mongoose = require('mongoose');
var bcrypt = require('bcryptjs');
var SALT_WORK_FACTOR = 10;


// user schema
var userSchema = new mongoose.Schema({
	email: {type: String, required: true},
	password: {type: String, default: ''},
	regDate: {type: Date, default: new Date()},
	provider: {type: String},
	providerId: {type: String},
	photo: {type: String},
	albums: {type: Array},
	google: {},
	vkontakte: {},
	github: {},
	facebook: {}
}, {collection: 'users'});

// hash the password before save
userSchema.pre('save', function (next) {
	var user = this;
	// only hash the password if it has been modified (or is new)
	if (!user.isModified('password')) return next();

	//generate a salt
	bcrypt.genSalt(SALT_WORK_FACTOR, function (err, salt) {
		if (err) return next(err);

		// hash the password along with our new salt
		bcrypt.hash(user.password, salt, function (err, hash) {
			if (err) return next(err);

			// override the clear text password with the hashed one
			user.password = hash;
			next();
		})
	})
});

// compare password
userSchema.methods.comparePassword = function (candidatePassword, cb) {
	bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
		if (err) return cb(err);
		cb(null, isMatch);
	});
};

// doc to Json method
userSchema.methods.toJSON = function () {
	var obj = this.toObject();
	delete obj.password;
	return obj;
};

// 'search by Name' method for user schema
userSchema.statics.findByEmail = function (email, cb) {
	return this.findOne({email: email}, cb);
};

exports.Account = mongoose.model('Account', userSchema);
