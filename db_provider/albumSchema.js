'use strict';

var mongoose = require('mongoose');

var albumSchema = new mongoose.Schema({
	title: {type: String, default: 'Album'},
	date: {type: Date, default: new Date()},
	description: {type: String},
	thumbnail: {type: String, default: '/css/img/thumbNail.png'},
	photos: {type: Array},
	quantity: {type: Number, default: 0},
	owner: {type: String}
}, {collection: 'albums'});

exports.Album = mongoose.model('Album', albumSchema);
