'use strict';

var mongoose = require('mongoose');
var config = require('../config/config');
var dbName = config.get('database.host')+config.get('database.name');
var conn = mongoose.createConnection(dbName);
var Grid = require ('gridfs-stream');
var gridform = require('gridform');

gridform.db = conn.db;
gridform.mongo = mongoose.mongo;
gridform.mongo.BSONPure = require('bson').BSONPure;

var gfs = Grid(conn.db, mongoose.mongo);

module.exports = {
	form: gridform,
	gfs: gfs
};
