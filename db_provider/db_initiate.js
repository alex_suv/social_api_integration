'use strict';

var config = require('../config/config');
var mongoose = require('mongoose');

var dbName = config.get('database.host')+config.get('database.name');

mongoose.connect(dbName);
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error constructor'));
db.once('open', function () {
	console.log('Mongoose connection open to ' + dbName);
});

exports.db = db;
