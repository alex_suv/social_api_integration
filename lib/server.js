'use strict';

var express = require('express');
var app = express();
var logger = require('connect-logger');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var passport = require('passport');
var config = require('../config/config');
// initialize db connection
require('../db_provider/db_initiate');
// initialize passport strategies
require('../passport/passport')(passport);
// using sessions
var session = require('express-session');
var MongoStore = require('connect-mongo')(session);
var store = new MongoStore({mongooseConnection: mongoose.connection});

// parse application/json
app.use(bodyParser.json());

// use logger
if (config.get('env') === 'development') {
	app.use(logger());
}

// configuring session middleware and mongo store
app.use(session({
	secret: config.get('session.secret'),
	key: 'sid',
	resave: false,
	saveUninitialized: true,
	cookie: {
		path: '/',
		httpOnly: true,
		maxAge: null
	},
	store: store
}));

app.use(passport.initialize());
app.use(passport.session());

// serving static files
app.use(express.static(__dirname + '/../public'));

// use auth routes
app.use(require('../handlers/auth/index.js'));
// use router
app.use(require('../handlers/index.js'));



//error handling
//extending response object with a sendHttpError method
express.response.sendHttpError = function (err) {
	this.status(err.status);
	this.json(err);
};

// errorConstructor handler
app.use(require('../errorConstructor/errorHandler'));

module.exports = app;
