(function () {
	'use strict';

	angular
		.module('social_api_integration')

		.factory('httpError', httpError);

	// http interceptor
	httpError.$inject = ['$q', '$rootScope'];
	function httpError($q, $rootScope) {
		return {
			// if error 401 received from server - broadcast event for authentication and reject
			responseError: function (rejection) {
				if (rejection.status === 401) {
					$rootScope.$broadcast('authenticationRequired');
					return $q.reject(new Error(rejection.data.message));
				}
			}
		};
	}
})();
