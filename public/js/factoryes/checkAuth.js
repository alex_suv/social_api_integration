(function () {
	'use strict';

	angular
		.module('social_api_integration')

		.factory('checkAuth', checkAuth);

	// checkAuthService for pages resolving
	checkAuth.$inject = ['$q', '$rootScope', '$http'];
	function checkAuth($q, $rootScope, $http) {
		return {
			//load user either from $rootScope either from the server
			check: function () {
				return $q.when($rootScope.currentUser ? $rootScope.currentUser :
						$http.get('/findUserBySession').success(function (res) {
							if (res.isLogged) {
								$rootScope.currentUser = helpers.stringToDate(res.user);
								return res;
							} else {
								return null;
							}
						})
				);
			}
		};
	}
})();
