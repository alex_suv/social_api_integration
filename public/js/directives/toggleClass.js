(function () {
	'use strict';

	angular
		.module('social_api_integration')

		.directive('toggleClass', toggleClass);

	function toggleClass() {
		return {
			restrict: 'A',
			link: linkFunc,
			controller: scopeController,
			controllerAs: 'scopeControllerVM',
			bindToController: true
		};

		function linkFunc (scope, element, attrs) {
			element.bind('click', function () {
				angular.element('#scroll-wrapper').toggleClass(attrs.toggleClass);
				scope.scopeControllerVM.isOpen = !scope.scopeControllerVM.isOpen;
				setTimeout(function () {
					scope.$apply();
				},300);
			});
		}
	}

	scopeController.$inject = [];
	function scopeController() {
		var vm = this;
		vm.isOpen = false;
	}
})();
