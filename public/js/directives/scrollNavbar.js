(function () {
	'use strict';

	angular
		.module('social_api_integration')

		.directive('scrollNavbar', scrollNavbar);

	function scrollNavbar() {
		return {
			restrict: 'A',
			link: linkFunc,
			controller: scrollNavbarCTRL,
			controllerAs: 'scrollNavbarVM',
			bindToController: true
		};

		function linkFunc (scope, element, attrs) {
			var lastScrollTop = 0;
			var body = angular.element('body');
			angular.element(window).scroll(function (e) {
				var st = $(this).scrollTop();
				if (st > lastScrollTop) {
					body.addClass('pageContentFullHeight');
				} else {
					body.removeClass('pageContentFullHeight');
				}
				lastScrollTop = st;
			})
		}
	}

	scrollNavbarCTRL.$inject = [];
	function scrollNavbarCTRL() {
		var vm = this;

	}
})();
