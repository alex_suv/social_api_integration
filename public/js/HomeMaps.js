function loadScript() {
	var script = document.createElement("script");
	script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&signed_in=true&callback=initialize';
	document.body.appendChild(script);
}

function initialize() {

	var myCenter = new google.maps.LatLng(51.508742, -0.120850);

	var mapProp = {
		center: myCenter,
		zoom: 7,
		mapTypeId: google.maps.MapTypeId.ROADMAP
	};
	google.maps.event.addDomListener(window, 'load', initialize);
	var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

	var marker=new google.maps.Marker({
		position:myCenter,
		animation:google.maps.Animation.DROP
	});

	marker.setMap(map);


}


window.onload = loadScript;