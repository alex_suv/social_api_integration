(function () {
	'use strict';

	angular
		.module('social_api_integration')

		.run(runBlock);

	// run block with redirecting to auth
	runBlock.$inject = ['$rootScope', '$state'];
	function runBlock($rootScope, $state) {
		$rootScope.activeConversations = [];
		//redirecting to authReq page
		$rootScope.$on('authenticationRequired', function () {
			$state.go('authRequired');
		});
	}
})();
