(function () {
	'use strict';

	angular
		.module('social_api_integration', ['ui.router', 'ngFileUpload'])

		.config(configure);


	// app configuring
	configure.$inject = ['$stateProvider', '$urlRouterProvider', '$httpProvider'];
	function configure($stateProvider, $urlRouterProvider, $httpProvider) {

		$urlRouterProvider.otherwise('/home');

		$stateProvider.
			state('home', {
				url: '/home',
				templateUrl: 'partials/home.html',
				controller: 'homeCTRL',
				controllerAs: 'homeCTRL',
				// load signed in user
				resolve: {
					user: function (checkAuth, $state, $q) {
						return checkAuth.check().then(function (res) {
							return res;
						}).catch(function (err) {
							console.error(err);
							$rootScope.$broadcast('authenticationRequired');
							return $q.reject(err);
						});
					}
				}
			}).

			state('signUp', {
				url: '/signUp',
				templateUrl: 'partials/signUp.html',
				controller: 'signUp',
				controllerAs: 'signUpCTRL'
			}).

			state('signIn', {
				url: '/signIn',
				templateUrl: 'partials/signIn.html',
				controller: 'signIn',
				controllerAs: 'signInCTRL'
			}).

			state('gallery', {
				url: '/gallery',
				templateUrl: 'partials/gallery.html',
				controller: 'gallery',
				controllerAs: 'galleryCTRL',
				resolve: {
					user: function (checkAuth, $state, $q) {
						return checkAuth.check().then(function (res) {
							return res;
						}).catch(function (err) {
							console.error(err);
							$rootScope.$broadcast('authenticationRequired');
							return $q.reject(err);
						});
					}
				}
			}).

			state('album', {
				url: '/album/:albumId',
				templateUrl: 'partials/album.html',
				controller: 'album',
				controllerAs: 'albumCTRL',
				resolve: {
					user: function (checkAuth, $state, $q) {
						return checkAuth.check().then(function (res) {
							return res;
						}).catch(function (err) {
							console.error(err);
							$rootScope.$broadcast('authenticationRequired');
							return $q.reject(err);
						});
					},
					albumId: function ($stateParams) {
						return $stateParams.albumId;
					}
				}
			}).

			state('authRequired', {
				url: '/forbidden',
				templateUrl: 'partials/authRequired.html'
			});

		//inserting httpError interceptor
		$httpProvider.interceptors.push('httpError');
	}
})();
