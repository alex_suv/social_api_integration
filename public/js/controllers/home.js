(function () {
	'use strict';

	angular
		.module('social_api_integration')

		.controller('homeCTRL', homeCTRL);

	homeCTRL.$inject = ['$state', '$http', '$rootScope'];

	function homeCTRL ($state, $http, $rootScope) {
		var vm = this;

		vm.signOut = signOut;

		function signOut () {
			$http.get('/signOut').success(function (res) {
				$rootScope.currentUser = null;
				vm.isLogged = false;
				$state.go('signIn');
			}).error(function (err) {
				console.error(err);
			})
		}
	}
})();
