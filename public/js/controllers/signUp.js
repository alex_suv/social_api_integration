(function () {
	'use strict';

	angular
		.module('social_api_integration')

		.controller('signUp', signUp);

	signUp.$inject = ['$state', '$http', '$rootScope'];

	function signUp ($state, $http, $rootScope) {
		var vm = this;
		vm.signUpButton = 'Sign up';
		vm.namePattern = /^\s*\w*\s*$/;
		vm.userSignUp = {};
		vm.message = {
			show: false,
			body: ''
		};

		vm.signUpUser = signUpUser;

		// log in by enter
		vm.onKeyPressLog = function ($event) {
			if ($event.which === 13 && !vm.signUpForm.$invalid) {
				vm.signUpUser();
			}
		};

		function signUpUser() {
			vm.signUpButton = 'Sign up...';
			var password = vm.userSignUp.password;
			var repPassowrd = vm.userSignUp.repPassword;
			if (password !== repPassowrd) {
				vm.message = {
					show: true,
					body: 'Password doesn\'t match the confirmation'
				};

				vm.userSignUp.password = '';
				vm.userSignUp.repPassword = '';
			} else {
				$http.post('/signUp', vm.userSignUp).success(function (res) {
					if (res.success) {
						$rootScope.currentUser = helpers.stringToDate(res.user);
						return $state.go('home');
					} else {
						vm.message = {
							show: true,
							body: res.body
						};
						vm.signUpButton = 'Sign up';
					}
				}).error(function (err) {
					console.error(err);
				});
				vm.userSignUp = {};
			}
		}

	}
})();
