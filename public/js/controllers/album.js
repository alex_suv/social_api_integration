(function () {
	'use strict';

	angular
		.module('social_api_integration')

		.controller('album', albumCTRL);

	albumCTRL.$inject = ['$state', '$http', '$rootScope', 'Upload', '$scope', 'albumId'];

	function albumCTRL($state, $http, $rootScope, Upload, $scope, albumId) {
		var vm = this;

		vm.photos = [];
		vm.newAlbum = {};
		vm.signOut = signOut;
		vm.upload = upload;
		getPhoto();

		$scope.$watch(function () {return vm.files}, function (files) {
			if (files) {
				if (!files.$error) {
					vm.upload(files);
				}
			}
		});

		function getPhoto() {
			var query = '?'+'albumId='+albumId;
			console.log(query);
			$http.get('/photo' + query)

				.success(function (res) {
					res.forEach(function (i) {
						i.queryname = '/image?' +'filename=' + i.filename;
					});
					vm.photos = res;
					console.log('received');
				})

				.error(function (err) {
					console.error(err);
				})
		}


		function upload(files) {

			Upload.upload({
				url: '/photo',
				fields: {albumId: albumId},
				file: files
			}).progress(function (e) {
				var progressPercentage = parseInt(100.0 * e.loaded / e.total);
				console.log(progressPercentage);
			}).success(function (data, status, headers, config) {
				getPhoto();
			}).error(function (data, status, headers, config) {
				console.log('error status: ' + status);
			})
		}

		function signOut() {
			$http.get('/signOut').success(function (res) {
				$rootScope.currentUser = null;
				vm.isLogged = false;
				$state.go('signIn');
			}).error(function (err) {
				console.error(err);
			})
		}
	}
})();
