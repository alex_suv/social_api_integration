(function () {
	'use strict';

	angular
		.module('social_api_integration')

		.controller('gallery', galleryCTRL);

	galleryCTRL.$inject = ['$state', '$http', '$rootScope', 'Upload', '$scope'];

	function galleryCTRL($state, $http, $rootScope, Upload, $scope) {
		var vm = this;

		vm.newAlbum = {};
		vm.signOut = signOut;
		vm.upload = upload;
		vm.createAlbum = createAlbum;
		vm.albums = [];
		getAlbums();

		$scope.$watch(function () {return vm.file}, function (file) {
			if (file) {
				if (!file.$error) {
					console.log(!file.$error);
					vm.upload(file);
				}
			}
		});

		function createAlbum () {
			console.log(vm.newAlbum);
			$http.put('/album', vm.newAlbum).success(function (res) {
				console.log(res);
				hideNewAlbumModal();
				getAlbums();
			}).error(function (err) {
				console.error(err);
			});
		}

		function getAlbums () {
			$http.get('/album').success(function (res) {
				console.log(res);

				res.map(function (i) {
					var dateObj = new Date(i.date);
					i.date = dateObj.toLocaleDateString();
					console.log(i);
					return i;
				});
				vm.albums = res;
			}).error(function (err) {
				console.error(err);
			});
		}

		function hideNewAlbumModal () {
			vm.newAlbum = {};
			angular.element('#newAlbumModel').modal('hide');
		}

		function upload(file) {
			Upload.upload({
				url: '/upload',
				fields: $rootScope.currentUser,
				file: file
			}).progress(function (e) {
				var progressPercentage = parseInt(100.0 * e.loaded / e.total);
				console.log(progressPercentage);
			}).success(function (data, status, headers, config) {
				console.log(data.files);
				vm.gallery = data.files;
			}).error(function (data, status, headers, config) {
				console.log('error status: ' + status);
			})
		}

		function signOut() {
			$http.get('/signOut').success(function (res) {
				$rootScope.currentUser = null;
				vm.isLogged = false;
				$state.go('signIn');
			}).error(function (err) {
				console.error(err);
			})
		}
	}
})();
