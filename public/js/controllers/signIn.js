(function () {
	'use strict';

	angular
		.module('social_api_integration')

		.controller('signIn', signIn);

	signIn.$inject = ['$state', '$http', '$rootScope'];

	function signIn ($state, $http, $rootScope) {
		var vm = this;
		vm.signInButton = 'Sign in';
		vm.namePattern = /^\s*\w*\s*$/;
		vm.userSignIn = {};
		vm.message = {
			show: false,
			body: ''
		};

		vm.signInUser = signInUser;

		// log in by enter
		vm.onKeyPressLog = function ($event) {
			if ($event.which === 13 && !vm.signIn.$invalid) {
				vm.signInUser();
			}
		};

		function signInUser() {
			console.log(vm.userSignIn);
			vm.signInButton = 'Sign in...';
			$http.post('/auth/login', vm.userSignIn).success(function (res) {
				console.log(res);

				if (res.success) {
					$rootScope.currentUser = helpers.stringToDate(res.user);
					return $state.go('home');
				} else {
					vm.message = {
						show: true,
						body: res.body
					};
					vm.signInButton = 'Sign in';
				}

			}).error(function (err) {
				console.error(err);
			})
		}

	}
})();
