(function (window) {
	window.helpers = window.helpers || {};

	// function for capitalize the first letters of the first and last names
	window.helpers.capitalizeFirstLetter = function (string) {
		return string.charAt(0).toUpperCase() + string.slice(1).toLocaleLowerCase();
	};

	// function to convert server response date prop values from string to Date
	window.helpers.stringToDate = function(user) {
		user.regDate = new Date(user.regDate);
		return user;
	};

})(window);
