module.exports = {
	env: {
		doc: "The applicaton environment.",
		format: ["production", "development", "test"],
		default: "development",
		env: "NODE_ENV"
	},
	bind: {
		ip: {
			doc: "The IP address to bind.",
			format: "ipaddress",
			default: "127.0.0.1",
			env: "IP_ADDRESS"
		},
		port: {
			doc: "The port to bind.",
			format: "port",
			default: 8888,
			env: "PORT",
			arg: "port"
		}
	},
	session: {
		secret: {
			doc: 'Secret key fot session storage',
			format: String,
			default: 'default'
		}
	},
	database: {
		host: {
			doc: "Database host name/IP",
			format: String,
			default: 'testing'
		},
		name: {
			doc: "Database name",
			format: String,
			default: 'users'
		}
	},
	redirectHost: {
		doc: "Redirect host",
		format: String,
		default: 'http://localhost:8888/#'
	},
	auth: {
		google: {
			GOOGLE_CLIENT_ID: {
				doc: "GOOGLE_CLIENT_ID",
				format: String,
				default: null
			},
			GOOGLE_CLIENT_SECRET: {
				doc: "GOOGLE_CLIENT_SECRET",
				format: String,
				default: null
			}
		},
		vkontakte: {
			VKONTAKTE_APP_ID: {
				doc: "VKONTAKTE_APP_ID",
				format: String,
				default: null
			},
			VKONTAKTE_APP_SECRET: {
				doc: "VKONTAKTE_APP_SECRET",
				format: String,
				default: null
			}
		},
		github: {
			GITHUB_CLIENT_ID: {
				doc: "GITHUB_CLIENT_ID",
				format: String,
				default: null
			},
			GITHUB_CLIENT_SECRET: {
				doc: "GITHUB_CLIENT_SECRET",
				format: String,
				default: null
			}
		},
		facebook: {
			FACEBOOK_APP_ID: {
				doc: "FACEBOOK_APP_ID",
				format: String,
				default: null
			},
			FACEBOOK_APP_SECRET: {
				doc: "FACEBOOK_APP_SECRET",
				format: String,
				default: null
			}
		}
	}
};
